package page

import (
	"encoding/json"
	"gopkg.in/macaron.v1"
	"strconv"
	"time"
)

type Params map[string]string

func (this Params) String(name, def string) string {
	if val, ok := this[name]; ok {
		return val
	}
	return def
}

func (this Params) Int(name string, def int) int {
	if val, ok := this[name]; ok {
		if v, e := strconv.Atoi(val); e == nil {
			return v
		}
	}
	return def
}

func (this Params) Int64(name string, def int64) int64 {
	if val, ok := this[name]; ok {
		if i, e := strconv.ParseInt(val, 10, 64); e == nil {
			return i
		}
	}
	return def
}

func Pager() macaron.Handler {
	return func(c *macaron.Context) {
		ctx := &Page{
			Context: c,
			Params:  Params{},
			Request: c.Req.Request,
			From:    time.Now(),
		}
		ctx.Req.ParseForm()
		if len(ctx.Req.Form) > 0 {
			for k, v := range ctx.Req.Form {
				ctx.Params[k] = v[0]
			}
		}
		c.Map(ctx)
	}
}

func ToJson(i interface{}) string {
	res, _ := json.Marshal(i)
	return string(res)
}
