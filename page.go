package page

import (
	"gopkg.in/macaron.v1"
	"net/http"
	"time"
)

type Page struct {
	*macaron.Context
	Request *http.Request
	Params  Params
	From    time.Time
}

func (this *Page) SetHeader(hdr string, val string, unique bool) {
	if unique {
		this.Header().Set(hdr, val)
	} else {
		this.Header().Add(hdr, val)
	}
}

func (this *Page) JSON(status int, dic interface{}) {
	this.SetHeader("Access-Control-Allow-Origin", "*", true)
	this.SetHeader("Content-Type", "application/json;charset=UTF-8", true)
	this.Context.JSON(status, dic)
}
